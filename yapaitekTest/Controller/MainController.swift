//
//  ViewController.swift
//  yapaitekTest
//
//  Created by salmani on 9/27/1399 AP.
//  Copyright © 1399 salmani. All rights reserved.
//

import UIKit
import Kingfisher

class MainController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var mainViewModel: MainViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callUpdateUI()
    }

    func callUpdateUI() {
        self.mainViewModel = MainViewModel()
        self.mainViewModel?.bindViewModelDataToController = { [weak self] in
            self?.collectionView.delegate = self
            self?.collectionView.dataSource = self
            self?.collectionView.reloadData()
        }
    }
}

extension MainController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainViewModel.photosCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath) as? GalleryCell
        let urlString =  "https://live.staticflickr.com/\(mainViewModel.galleryData.photos.photo[indexPath.row].server)/\(mainViewModel.galleryData.photos.photo[indexPath.row].id)_\(mainViewModel.galleryData.photos.photo[indexPath.row].secret).jpg"
        cell?.imgFlicker.kf.setImage(with: URL(string: urlString ?? ""))
        cell?.lblDetail.text = mainViewModel.galleryData.photos.photo[indexPath.row].title
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! GalleryCell
        let selectedImage = mainViewModel.galleryData.photos.photo[indexPath.item]
        cell.flipCard(view: cell.contentView)
    }    
}
