//
//  APISerivce.swift
//  yapaitekTest
//
//  Created by salmani on 9/27/1399 AP.
//  Copyright © 1399 salmani. All rights reserved.
//

import Foundation
import Alamofire

class APIServices {
    //Http Request
    func sendRequest(url: String, method: HTTPMethod, onResponse: @escaping (_ response: FlickerResponse) -> Void) {
        if isConnectedToInternet() {
            AF.request(url, method: method, parameters: nil, encoding: JSONEncoding.default, headers: nil)
                .responseData { response in
//                    print("🌐 (RESPONSE) METHOD: \(method) URL: \(FlickerApi.photosPath + url) IN: [\(Date())]")
                    //print(String(data: response.data!, encoding: .utf8)!)
                    switch response.result {
                    case .success:
                        if response.value != nil {
                            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                onResponse(FlickerResponse(result: data, error: false, errorType: nil, responseCode: response.response?.statusCode))
                            }
                        } else {
                            onResponse(FlickerResponse(result: nil, error: true, errorType: .nullResponse, responseCode: response.response?.statusCode))
                        }
                        
                        break
                    case .failure(let error):
                        print(error)
                        onResponse(FlickerResponse(result: nil, error: true, errorType: .connectToServer, responseCode: response.response?.statusCode))
                    }
                }
            } else {
                onResponse(FlickerResponse(result: nil, error: true, errorType: .noInternet, responseCode: nil))
        }
    }
    
    private func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
