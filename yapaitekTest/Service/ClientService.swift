//
//  ClientService.swift
//  yapaitekTest
//
//  Created by salmani on 9/27/1399 AP.
//  Copyright © 1399 salmani. All rights reserved.
//

import Foundation

class ClientService: NSObject {
    private var api = APIServices()

    func getPhotosData(pageCount: String, tag: String ,onResponse: @escaping (_ response: FlickerResponse) -> Void){
        api.sendRequest(url: FlickerApi.photosPath + "&format=json&nojsoncallback=1&safe_search=1&text=\(tag)&page=\(pageCount)", method: .get) { (response) in
            if response.error == false {
                let decoder = JSONDecoder()
                if let parsed = try? decoder.decode(Gallery.self, from: response.result as! Data) {
                    if parsed.stat == "ok" {
                        onResponse(FlickerResponse(result: parsed, error: false, errorType: nil, responseCode: response.responseCode))
                    } else {
                        onResponse(FlickerResponse(result: nil, error: false, errorType: nil, responseCode: response.responseCode))
                    }
                } else {
                    onResponse(FlickerResponse(result: nil, error: false, errorType: nil, responseCode: response.responseCode))
                }
            } else {
                onResponse(response)
            }
        }
    }
}
