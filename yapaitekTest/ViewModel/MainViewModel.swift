//
//  MainViewModel.swift
//  yapaitekTest
//
//  Created by salmani on 9/27/1399 AP.
//  Copyright © 1399 salmani. All rights reserved.
//

import Foundation

class MainViewModel: NSObject {
    private var cs: ClientService!
    var pageCount = 1
    var photosCount : Int = 0
    private(set) var galleryData: Gallery! {
        didSet {
            self.bindViewModelDataToController()
        }
    }
    
    var bindViewModelDataToController: (() -> ()) = {}
    
    override init() {
        super.init()
        self.cs = ClientService()
        callToGetGalleryData(searchText: "Kitten")
    }
    
    func callToGetGalleryData(searchText: String) {
        self.cs?.getPhotosData(pageCount: String(pageCount + 1), tag: searchText, onResponse: { (response) in
            if response.error == false {
                print("going to page: \(self.pageCount)")
                if let res = response.result as? Gallery {
                    self.galleryData = res
                    self.setItemCount(items: res.photos.photo.count)
                    self.galleryData.photos.photo.append(contentsOf: res.photos.photo)
                } else {
                    print("unabled to load data")
                }
            } else {
                print(response.errorType!)
            }
        })
    }
    
    func setItemCount (items: Int) {
        self.photosCount = items
    }
    
    func photoDataAt(indexPath: IndexPath) -> GalleryData? {
        if indexPath.row >= 0 && indexPath.row < photosCount {
            return galleryData.photos.photo[indexPath.row]
        }
        return nil
    }
    
    func updateSearchResult(with photo: [GalleryData]){
        DispatchQueue.main.async { [unowned self] in
            let newItems = photo
            self.galleryData.photos.photo.append(contentsOf: newItems)
        }
    }
}
