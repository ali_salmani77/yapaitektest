//
//  GalleryCell.swift
//  yapaitekTest
//
//  Created by salmani on 9/28/1399 AP.
//  Copyright © 1399 salmani. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    @IBOutlet var imgFlicker: UIImageView!
    @IBOutlet var lblDetail: UILabel!
    
    func flipCard(view: UIView) {
        if lblDetail.isHidden {
            flipAnimate(with: view)
            lblDetail.isHidden = false
            imgFlicker.isHidden = true
        } else {
            imgFlicker.isHidden = false
            flipAnimate(with: view)
            lblDetail.isHidden = true
        }
    }
    
    func flipAnimate(with view: UIView) {
        let transitionOptions = UIView.AnimationOptions.transitionFlipFromLeft
        UIView.transition(with: view,
                          duration: 0.3,
                          options: transitionOptions,
              animations: nil,
              completion: nil)
    }
}
