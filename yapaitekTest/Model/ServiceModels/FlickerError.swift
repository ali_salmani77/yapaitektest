//
//  FlickerError.swift
//  yapaitekTest
//
//  Created by salmani on 9/27/1399 AP.
//  Copyright © 1399 salmani. All rights reserved.
//
import Foundation

enum FlickerError: Error{
    case noInternet
    case connectToServer
    case parseJson
    case nullResponse
    case actionFailed
    case databaseFetch
}
