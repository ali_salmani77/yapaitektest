//
//  FlickerResponse.swift
//  yapaitekTest
//
//  Created by salmani on 9/27/1399 AP.
//  Copyright © 1399 salmani. All rights reserved.
//
import Foundation

struct FlickerResponse {
    var result: Any?
    var error: Bool
    var errorType: FlickerError?
    var responseCode: Int?
}
