//
//  Gallery.swift
//  yapaitekTest
//
//  Created by salmani on 9/27/1399 AP.
//  Copyright © 1399 salmani. All rights reserved.
//

import Foundation

struct Gallery: Codable {
    var photos: GallerySearchPageResponse
    var stat: String
}

struct GallerySearchPageResponse: Codable {
    var page: Int
    var pages: Int
    var perpage: Int
    var total: String
    var photo: [GalleryData]
}

struct GalleryData: Codable {
    let id: String
    let owner: String
    let secret: String
    let server: String
    let farm : Int
    let title: String
    let ispublic: Int
    let isfriend: Int
    let isfamily: Int
}
